# Fakenote

## Overview

In this project you are to develop a Facebook alike platform using `Angular`, `Node.js` and `MySQL` within 2 weeks.

Your platform should include the following features:

- Basic User Feature (registration, login, logout, add friend, delete friend)
- Dashboard Feature (Create Post, Delete Post, Modify Post, Show Posts)

## Architecture Guideline

In this project you will have at least three components:

- An API server written in `Node.js` language using [Nest](https://docs.nestjs.com/) as framework for handling most of the user request
- A `MySQL` database to store user's data
- A Web GUI written with [Angular](https://angularjs.org/) for user to interact

### A Simple Work Flow

```plantuml
GUI -> APIServer : GET User
APIServer -> Database : SELECT * FROM User;
Database -> APIServer : | UserName1 | UserId1 | ... |
APIServer -> GUI : 200 OK {"userName": "UserName1", "id": "UserId1"}
```

## Specification Guideline

The project can be marked as **partially finish** if Basic & Dashboard features are implemented in APIServer and Database. It is to be marked as **completely finish** if the three components works seamlessly. However, there are some tasks that *can be added* to make this project more complete and professional, such as:

- Documentation using `Swagger` (plugin available natively in Nest)
- Database connection using `TypeORM` (plugin available natively in Nest)
- `Dockerize` the services into three containers for each service

## Note

- All data between API Server and Web GUI must use HTTPs
- The API server should implement its API using REST
- Angular must use scss as style language
- Please comment and document your code for easy scanning
- The Dashboard feature(Create Post, Delete Post, Modify Post, Show Posts) should be able to be used on all friend's dashboard
- During the implementation of this project, please use Git to version control your code
- Inside the source of each project, you should have at least a `README.md` file to guide how to deploy the service, as well as stating the problem you've encountered and how you solved it
- You must use eslint as coding style linter (or follow this [coding style guide](https://github.com/Microsoft/TypeScript/wiki/Coding-guidelines))

## Demo and Presentation

Please push your work as a public repositories on Github, BitBucket, or Gitlab. You need to inform us the project link at least 2 working days before your presentation day.
